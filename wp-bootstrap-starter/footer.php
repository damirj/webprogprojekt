<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
		</div><!-- .container -->
	</div><!-- #content -->
    <?php get_template_part( 'footer-widget' ); ?>

	<!-- footer preko columna-->
		<div class="container">
			<div class="row d-flex justify-content-center">
				<?php
					if( have_rows('linkovi_iznad_footera', 15600) ){
						$Linkovi = get_field_object('linkovi_iznad_footera', 15600);
						$brojLinkova = count($Linkovi['value']);
						for ($i=0; $i < $brojLinkova; $i++) {
							the_row(); ?>

							<div class="linkovi-iznad-footera">
								<a href="<?php the_sub_field('url');?>" target="_blank"><?php the_sub_field('naziv'); ?></a>
							</div>
							<?php
								if(($i+1) != $brojLinkova){ ?>
									<span class="linkovi-iznad-footera">|</span>
					<?php	} ?>

							<?php
						}
					}
				?>
			</div>
			<div class="row d-flex justify-content-end">
				<div class="col-12 col-md-11 d-flex justify-content-end footerpadding footer-container-mobile">
					<?php
						if( have_rows('footer_icons', 15600) ){ ?>

							<div class="align-self-center d-flex justify-content-center footericon">
								<?php the_row(); ?>
								<img src="<?php the_sub_field('icon'); ?>" style="width:90px;height:134px;"/>
							</div>

							<div class="align-self-center d-flex justify-content-center footericon">
								<?php the_row(); ?>
								<img src="<?php the_sub_field('icon'); ?>" style="width:130px;height:113px;"/>
							</div>

							<div class="align-self-center d-flex justify-content-center footericon">
								<?php the_row(); ?>
								<img src="<?php the_sub_field('icon'); ?>" style="width:83px;height:116px;"/>
							</div>

							<div class="align-self-center d-flex justify-content-center footericon">
								<?php the_row(); ?>
								<img src="<?php the_sub_field('icon'); ?>" style="width:165px;height:68px;"/>
							</div>

							<div class="align-self-center d-flex justify-content-center footericon">
								<?php the_row(); ?>
								<img src="<?php the_sub_field('icon'); ?>" style="width:370.55px;height:126.92px;"/>
							</div>

				  <?php } ?>

				</div>
				
				<div class="col-12 d-flex justify-content-center prava"> 
					<p>Sva prava pridržana &#169 <?php echo date("Y"); ?>. - Abeceda pismenosti - Izrada: <a href="https://www.ofir.hr/" target="_blank"><span>Ofir d.o.o.</span></a></p>
				</div>
				
			</div>
		</div>
	<!--KRAJ Footer-a-->

		<?php /*	<footer id="colophon" class="site-footer <?php echo wp_bootstrap_starter_bg_class(); ?>" role="contentinfo">
				<div class="container-fluid p-3 p-md-5">
					<div class="site-info">
						&copy; <?php echo date('Y'); ?> <?php echo '<a href="'.home_url().'">'.get_bloginfo('name').'</a>'; ?>
						<span class="sep"> | </span>
						<a class="credits" href="https://afterimagedesigns.com/wp-bootstrap-starter/" target="_blank" title="Wordpress Technical Support" alt="Bootstrap Wordpress Theme"><?php echo esc_html__('Bootstrap Wordpress Theme','wp-bootstrap-starter'); ?></a>

					</div><!-- close .site-info -->
				</div>
			</footer><!-- #colophon -->  */ ?>
		<?php endif; ?>

		</div><!--END OF background img div "Pozadina slika"-->
	</div> <!--END OF background div-->
</div><!-- #page -->

<script>
  equalheight = function(container){

  var currentTallest = 0,
    currentRowStart = 0,
    rowDivs = new Array(),
    $el,
    topPosition = 0;
   jQuery(container).each(function() {

     $el = jQuery(this);
     jQuery($el).height('auto')
     topPostion = $el.position().top;

     if (currentRowStart != topPostion) {
    for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
      rowDivs[currentDiv].height(currentTallest);
    }
    rowDivs.length = 0; // empty the array
    currentRowStart = topPostion;
    currentTallest = $el.height();
    rowDivs.push($el);
     } else {
    rowDivs.push($el);
    currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
    }
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
    rowDivs[currentDiv].height(currentTallest);
     }
   });
  }
	 jQuery(document).ready(function() {
		equalheight('.content-height');
	  });
  jQuery(window).load(function() {
    equalheight('.content-height');
  });


  jQuery(window).resize(function(){
    equalheight('.content-height');
  });
 </script>

 <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
 <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
 <script type="text/javascript" src="<?php bloginfo('url')?>/wp-content/themes/wp-bootstrap-starter/slick/slick.min.js"></script>

<script type="text/javascript">
jQuery(document).ready(function(){
  jQuery('.your-class').slick({
		prevArrow:"<img class='prev slick-prev' src='<?php bloginfo('url')?>/wp-content/themes/wp-bootstrap-starter/images/arrow-left-slider.png'>",
		nextArrow:"<img class='next slick-next' src='<?php bloginfo('url')?>/wp-content/themes/wp-bootstrap-starter/images/arrow-right-slider.png'>"
  });
});
</script>



<?php wp_footer(); ?>

</body>
</html>
