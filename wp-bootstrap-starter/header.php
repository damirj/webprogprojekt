<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('url')?>/wp-content/themes/wp-bootstrap-starter/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('url')?>/wp-content/themes/wp-bootstrap-starter/slick/slick-theme.css"/>
	<link rel="icon" href="<?php bloginfo('url') ?>/wp-content/themes/wp-bootstrap-starter/images/favicon.png" type="image/png">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site IEoverflow">

	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wp-bootstrap-starter' ); ?></a>
    <?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
	<header id="masthead" class="site-header navbar-static-top <?php echo wp_bootstrap_starter_bg_class(); ?>" role="banner">

    <div class="container">
		<!--Top header -->
			<div class="row">
				<div class="col-8 col-md-4 align-self-center abeceda-logo">

					<?php /*<div class="navbar-brand">
							<?php  if ( get_theme_mod( 'wp_bootstrap_starter_logo' ) ): ?>
								<a href="<?php echo esc_url( home_url( '/' )); ?>">
									<img src="<?php echo esc_attr(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
								</a>
							<?php else : ?>
								<a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>"><?php esc_url(bloginfo('name')); ?></a>
							<?php endif;  ?>
						</div>  */ ?>

				<!--LOGO ikona-->
				<a href="<?php the_field('logo_url_header', 15600); ?>"><img class="mobile-size-img" src="<?php the_field('logo_icon_header', 15600) ?>" /></a>
				<!--KRAJ LOGO ikone-->
				</div>

				<div class="col-12 col-md-4">
				<!--PRAZNO-->
				</div>

				<div class="col-12 col-md-4 align-self-center header-partneri-mobile-padding">
					<div class="row d-flex justify-content-end header-partneri-mobile">
					<!--Custom field za header ikone i URL, top header, desna strana. -->
						<?php
							if( have_rows('header_icons', 15600) ):

								// loop through the rows of data
								while ( have_rows('header_icons', 15600) ) : the_row();
									// display a sub field value ?>

								<div class="align-self-center headericon">
									<a href="<?php the_sub_field('link');?>" target="_blank"><img class="mobile-size-img" src="<?php the_sub_field('icon'); ?>" /></a>
								</div>

									<?php
								endwhile;

							else :
								// no rows found
							endif;
						?>
					<!--KRAJ Custom field-a za header ikone i URL -->
					</div>

					<div class="row">
					<!--Drugi red top header-a, desno, custom field-->
						<div class="col-12 header-partneri-mobile header-partneri-mobile-padding" id="headerIconSecondRow">
						<img class="mobile-size-img-eu" src="<?php the_field('header_drugi_red_ikona', 15600) ?>" />
						</div>
					<!--KRAJ Drugog reda na top header-u-->
					</div>
				</div>

			</div>
			<!--KRAJ Top header-a-->
		</div>
			<!--Bottom header-->
			<div class="topborder">
				<div class="container">
					<nav class="navbar navbar-expand-md p-0 hamburger-mobile">

						<button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>

						<!--<div class="collapse navbar-collapse col-sm-12 hamburger-menu">-->
            <div class="col-sm-12 hamburger-menu">

							<?php
							wp_nav_menu(array(
							'theme_location'    => 'primary',
							'container'       => 'div',
							'container_id'    => '',
							'container_class' => 'collapse navbar-collapse justify-content-end hamburger-drop',
							'menu_id'         => false,
							'menu_class'      => 'navbar-nav',
							'depth'           => 3,
							'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
							'walker'          => new wp_bootstrap_navwalker()
							));
							?>
						</div>

					</nav>
				</div>
			</div>
			<!--KRAJ Bottom header-->
	</header><!-- #masthead -->


	<!--HEADER BANNER-->
    <?php /* if(is_front_page() && !get_theme_mod( 'header_banner_visibility' )): ?>
        <div id="page-sub-header" <?php if(has_header_image()) { ?>style="background-image: url('<?php header_image(); ?>');" <?php } ?>>
            <div class="container">
                <h1>
                    <?php
                    if(get_theme_mod( 'header_banner_title_setting' )){
                        echo get_theme_mod( 'header_banner_title_setting' );
                    }else{
                        echo 'Wordpress + Bootstrap';
                    }
                    ?>
                </h1>
                <p>
                    <?php
                    if(get_theme_mod( 'header_banner_tagline_setting' )){
                        echo get_theme_mod( 'header_banner_tagline_setting' );
                }else{
                        echo esc_html__('To customize the contents of this header banner and other elements of your site, go to Dashboard > Appearance > Customize','wp-bootstrap-starter');
                    }
                    ?>
                </p>
                <a href="#content" class="page-scroller"><i class="fa fa-fw fa-angle-down"></i></a>
            </div>
        </div>
    <?php endif; */?>
	<!--KRAJ HEADER BANNER-->

			<div class="backgroundBody"><!--START OF background-->
			<div class="backgroundBodyImg" style="background-image:url(<?php the_field('pozadina_slika', 15600); ?>);"><!--START OF background img "Pozadina slika"-->

	<?php endif; ?>
