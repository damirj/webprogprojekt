<?php
/* Template Name: Oprojektu */
?>

<?php get_header(); ?>

<?php// get_header(); ?>
<?php //putRevSlider("slider1"); ?>

<?php if( have_rows('slider', 15600) ){ ?>
		<div class="your-class">
			<?php while( have_rows('slider', 15600) ){
					the_row(); 	?>
					<div>
						<?php if(get_sub_field('slider_slika')){ ?>
										<img src="<?php the_sub_field('slider_slika'); ?>" width="100%" height="auto" />
						<?php } ?>
						<?php if(get_sub_field('tekst_na_slideru')){ ?>

							<div class="slider-tekst">
								<?php the_sub_field('tekst_na_slideru'); ?>
								<?php if(get_sub_field('dugme_url') && get_sub_field('dugme_tekst')){ ?>
								<a class="slider-button" href="<?php the_sub_field('dugme_lokacija'); ?>"><?php the_sub_field('dugme_tekst'); ?></a>
								<?php } ?>
							</div>
				<?php } ?>
				 </div>

			<?php } ?>
		</div>
<?php } ?>




<div id="content" class="site-content">
	<div class="container" id="contentID">
		<div class="row novostiPost">
<h1 class="naslov-h1"><?php the_title(); ?></h1>
			<div class="container projekt-container">
				
				<?php the_field('opis'); ?>
				<hr class="borderPress1 projektHRpadding">
				<div class="col-12 d-flex justify-content-center projektBottomContent">
					<div class="row d-flex justify-content-center slika-preuzimanje">
						<?php 
						$glavna = get_field('glavna_datoteka');
						if($glavna){ ?>
							<a href="<?php echo $glavna['datoteka']; ?>" target="_blank"><img src="<?php echo $glavna['slika_preuzimanje']; ?>" class="projektSlika contentShadow" ></a>
						<?php } ?>
						
					</div>
					
						<?php 
						if( have_rows('preuzimanje_datoteka') ){

										// loop through the rows of data
										while ( have_rows('preuzimanje_datoteka') ){ ?>
										<div class="row d-flex justify-content-end pdf-preuzimanje">
										<div class="col-12 col-lg-8 col-md-9 col-sm-10 pdf-sporedni">
										<?php
											the_row();
											$url = get_sub_field('preuzimanje');
											$pathinfo = pathinfo($url);
											
											
											if($pathinfo['extension'] == "pdf"){ ?>
											
												<a href="<?php the_sub_field('preuzimanje'); ?>" target="_blank" class="projektLink align-self-end"><img src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/04/pdf_file.png" width="45px"><?php the_sub_field('naziv_datoteke'); ?></a>
									  
									  <?php }elseif( $pathinfo['extension'] == 'doc' || $pathinfo['extension'] == 'docx' ){ ?>
									  
												<a href="<?php the_sub_field('preuzimanje'); ?>" target="_blank" class="projektLink align-self-end"><img src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/04/doc_file.png" width="45px"><?php the_sub_field('naziv_datoteke'); ?></a>
											
									  <?php }elseif( $pathinfo['extension'] == 'xls' || $pathinfo['extension'] == 'xlsx' ){ ?>
									  
												<a href="<?php the_sub_field('preuzimanje'); ?>" target="_blank" class="projektLink align-self-end"><img src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/04/xls_file.png" width="45px"><?php the_sub_field('naziv_datoteke'); ?></a>
											
									  <?php }elseif( $pathinfo['extension'] == 'ppt' || $pathinfo['extension'] == 'pptx' ){ ?>
									  
												<a href="<?php the_sub_field('preuzimanje'); ?>" target="_blank" class="projektLink align-self-end"><img src="<?php bloginfo('url'); ?>/wp-content/uploads/2018/04/ppt_file.png" width="45px"><?php the_sub_field('naziv_datoteke'); ?></a>
											
									  <?php } ?>
											</div>									  
											</div>
											<?php
										}

									
										
						}
						?>
					
				</div>
			</div>


<?php
get_footer();
?>
