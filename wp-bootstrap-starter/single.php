<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

<div id="content" class="site-content">
	<div class="container" id="contentID single-post-contentID">
		<div class="row novostiPost d-flex justify-content-center">

			<section id="primary" class="content-area col-sm-12">
				<main id="main" class="site-main" role="main">

					<?php
						while ( have_posts() ){
							the_post();
						?>
							<article class="linkTitle artikal-razmak" id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
								<?php
								//$var= get_the_category(get_the_ID());
								//print_r($var);

								the_title('<h2 class="clanak-naslov-margin">', '</h2>');
									  /*if( has_post_thumbnail() ){ ?>
										<div class="singleImg"><?php the_post_thumbnail('full'); ?></div>
								<?php }  */?>

								<?php /* if(has_post_thumbnail()){ ?>
										<div class="singleImg"><?php the_post_thumbnail('full'); ?></div>
								<?php }else{ ?>
										<div class="singleImg"><img src="https://praksa.braco.does-it.net/damir/abecedapismenosti/wp-content/uploads/2018/03/abeceda-default-slika.jpg"></div>
									<?php } */ ?>
									
								<?php if(get_field('istaknuta_slika')){ ?>
										<div class="singleImg"><img src="<?php the_field('istaknuta_slika') ?>"></div>
								<?php }else{ ?>
										<div class="singleImg"><img src="https://praksa.braco.does-it.net/damir/abecedapismenosti/wp-content/uploads/2018/03/abeceda-default-slika.jpg"></div>
									<?php } ?>


						<?php if(get_field('foto-auto')){ ?>
										<small class="foto-autor"><?php the_field('foto_autor'); ?></small><br>
						<?php } ?>
									<small>Objavljeno: <strong><?php the_time( 'j. F Y' ); ?></strong></small>
									<hr class="borderPress1 hrSingle">

									<div class="row d-flex justify-content-end">
										<div class="col-12 col-md-11 text-left clanak-padding">
											<?php the_content(); ?>
											<?php while(have_rows('video_youtube')){
													the_row();?>
													<div class="embed-container">
														<?php the_sub_field('youtube_video'); ?>
													</div>
											<?php } ?>
											<?php the_field('video_upload'); ?>

											<?php while(have_rows('pdf_datoteka')){ the_row(); ?>
													<a class="projektLink align-self-end" target="_blank" href="<?php the_sub_field('datoteka_pdf');?>"><img src="<?php the_field('ikona_za_preuzimanje',15786); ?>"><?php the_sub_field('naziv_pdf'); ?></a>
											<?php } ?>
										</div>
									</div>
							</article>
						<?php
							$prev_post = get_adjacent_post(true, '', true);
							$next_post = get_adjacent_post(true, '', false);
							if(!empty($prev_post) || !empty($next_post) ){ ?>
								<div class="row d-flex justify-content-end">
									<div class="col-12 col-md-9 clanakNavBorder">
										<div class="row navigacija-red">
										<?php
											$prev_post = get_adjacent_post(true, '', true);
											if(!empty($prev_post)) { ?>
												<div class="col-6 d-flex justify-content-start navigacija-clanak">
													<a class="strelice" href="<?php echo get_permalink($prev_post->ID); ?>" title="<?php $prev_post->post_title ?>">
														<div><img src="https://praksa.braco.does-it.net/damir/abecedapismenosti/wp-content/uploads/2018/03/leftarrow.png"> PROŠLA</div>
													</a>
												</div>
											<?php
											}else{ ?>
												<div class="col-6 d-flex">
												</div>
								<?php }
											$next_post = get_adjacent_post(true, '', false);
											if(!empty($next_post)) { ?>
												<div class="col-6 d-flex justify-content-end navigacija-clanak">
													<a class="strelice" href="<?php echo get_permalink($next_post->ID); ?>" title="<?php $next_post->post_title ?>">
														<div >SLJEDEĆA <img src="https://praksa.braco.does-it.net/damir/abecedapismenosti/wp-content/uploads/2018/03/rightarrow.png"></div>
													</a>
												</div>
											<?php } ?>
										</div>
											<?php
												$args = array(
												'in_same_term'=>true,
												);
												the_post_navigation($args);
											?>
									</div>
								</div>

				<?php	}
						}
						?>

				</main><!-- #main -->
			</section><!-- #primary -->
		</div>
<?php
get_footer();
