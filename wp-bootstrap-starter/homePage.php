<?php
/* Template Name: Homepage */
?>

<?php get_header(); ?>





<?php if( have_rows('slider', 15600) ){ ?>
				<div class="your-class">
					<?php while( have_rows('slider', 15600) ){
							the_row(); 	?>
							<div>
					<?php if(get_sub_field('slider_slika')){ ?>
									<img src="<?php the_sub_field('slider_slika'); ?>" width="100%" height="auto" />
					<?php } ?>
					<?php if(get_sub_field('tekst_na_slideru')){ ?>

									<div class="slider-tekst">
										<?php the_sub_field('tekst_na_slideru'); ?>
										<?php if(get_sub_field('dugme_url') && get_sub_field('dugme_tekst')){ ?>
										<a class="slider-button" href="<?php the_sub_field('dugme_lokacija'); ?>"><?php the_sub_field('dugme_tekst'); ?></a>
										<?php } ?>
									</div>

					<?php } ?>
					   	</div>

				<?php } ?>
				</div>
<?php } ?>





<div id="novosti" class="container-fluid d-flex h-110" >
	<div class="container d-flex h-110" id="ispodslidera">
		<div class="row align-self-center">
			<div class="col-9 col-sm-6 col-md-4 col-lg-4 d-flex align-items-center">
				<span class="ispod-slidera-naslov"><a href="novosti/">Novosti i press</a></span>
				<a class="ikona-banner" href="novosti/"><img src="<?php the_field('glifikon', 15600); ?>"></a>
			</div>
		</div>
	</div>
</div>


<div id="content" class="site-content">
	<div class="container" id="contentID">
		<div class="row novostiPost">
			<!--NOVOSTI-->
			<div class="col-12 col-xl-8 d-flex justify-content-center">
				<?php
					$query = new WP_Query( 'cat=51' );
					if ( $query->have_posts() ) { ?>
						<ul class="novostiPostUL">
						<?php
							for($i=0; $i<2; $i++){
								if ( $query->have_posts() ) {
									$query->the_post(); ?>
									<li class="novostiPostLI">
										<div class="post">
											<div class="entry">
												<div class="novostiImage">
													<?php /*
													if(has_post_thumbnail()){?>
														<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
											  <?php }else{ ?>
														<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><img src="https://praksa.braco.does-it.net/damir/abecedapismenosti/wp-content/uploads/2018/03/abeceda-default-slika.jpg"></a>
													<?php } */?>
													
													
													<?php if(get_field('istaknuta_slika')){ ?>
														<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><img src="<?php the_field('istaknuta_slika'); ?>"></a>
													<?php }else{ ?>
														<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><img src="wp-content/themes/wp-bootstrap-starter/images/abeceda-default-slika.jpg"></a>
													<?php } ?>

												</div>
												<div class="novost-content novostiContent">
													<div class="content-height">
														<h4><a class="linkTitle" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
														<small class="objavljenoPost">Objavljeno:<b> <?php the_time( 'j. F Y' ); ?></b> </small><br><br>
														<?php
															the_field("kratki_opis");
														?>
														<br>
														<a href="<?php the_permalink() ?>"><img class="novosti-vise" src="<?php the_field('glifikon_2', 15600); ?>"></a>
													</div>
												</div>
											</div>
										</div>
									</li>
						  <?php }
							} ?>
						</ul>
					<?php
					 wp_reset_postdata();
					}
					 else {
					 ?>
					 <p><?php //esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
				 <?php } ?>

			</div>
			<!--END OF NOVOSTI-->

			 <!-- PRESS -->
			<div class="col-12 col-xl-4 press-stupac">
				<?php
					$query = new WP_Query( 'cat=52&posts_per_page=-1' );
					if ( $query->have_posts() ) {
						?>
						<div class="contentShadow pressMargin">
						<span  id="pressNaslov">Press</span>

							<hr class="borderPress">
							<?php
							for($i=0; $i<3; $i++){
								if ( $query->have_posts() ) {
									$query->the_post(); ?>

									<div class="postPress">
										<div class="pressPadding">
											<!-- Display the date (November 16th, 2009 format) and a link to other posts by this posts author. -->
											<small class="pressDate"><?php the_time( 'j. F Y' ); ?> </small>

											<?php if(get_field('video_youtube') || get_field('video_upload')){ ?>
													<span class="play-btn">
														<img src="<?php bloginfo('url') ?>/wp-content/themes/wp-bootstrap-starter/images/video-play1.jpg" >
													</span>
											<?php } ?>

											<!-- Display the Title as a link to the Post's permalink. -->
											<h4 class="h4-inline"><a class="linkTitle" href="<?php the_permalink() ?>" rel="bookmark" title="Poveznica na <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
										</div>
									<hr>
									</div> <!-- closes the first div box -->

						  <?php }
							} ?>
						</div>
			  <?php wp_reset_postdata();
					}
					 else {
					 ?>
					 <p><?php //esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php } ?>
			</div>
			<!--END OF PRESS-->

		</div>	<!--END row -->

		<div class="row justify-content-end">
			<div class="col-sm-12 col-md-10">
			<?php the_field('tekst_ispod_posta'); ?>
			</div>
		</div>



<?php

get_footer();
?>
