<?php
/* Template Name: Novosti */
?>

<?php get_header(); ?>
	<?php //putRevSlider("slider1"); ?>

	<?php if( have_rows('slider', 15600) ){ ?>
			<div class="your-class">
				<?php while( have_rows('slider', 15600) ){
						the_row(); 	?>
						<div>
							<?php if(get_sub_field('slider_slika')){ ?>
											<img src="<?php the_sub_field('slider_slika'); ?>" width="100%" height="auto" />
							<?php } ?>
							<?php if(get_sub_field('tekst_na_slideru')){ ?>

								<div class="slider-tekst">
									<?php the_sub_field('tekst_na_slideru'); ?>
									<?php if(get_sub_field('dugme_url') && get_sub_field('dugme_tekst')){ ?>
									<a class="slider-button" href="<?php the_sub_field('dugme_lokacija'); ?>"><?php the_sub_field('dugme_tekst'); ?></a>
									<?php } ?>
								</div>
					<?php } ?>
				   </div>

				<?php } ?>
			</div>
	<?php } ?>





<div id="novosti" class="container-fluid d-flex h-110" >
	<div class="container d-flex h-110" id="ispodslidera">
		<div class="row align-self-center">
			<div class="col-9 col-sm-6 col-md-4 col-lg-4 d-flex align-items-center">
				<span class="ispod-slidera-naslov"><a href="novosti/">Novosti i press</a></span>
				<a class="ikona-banner" href="novosti/"><img src="<?php the_field('glifikon', 15600); ?>"></a>
			</div>
		</div>
	</div>
</div>

<div id="content" class="site-content">
	<div class="container" id="contentID">
		<div class="row novostiPost">

			<!--NOVOSTI-->
			<div class="col-12 col-xl-8">
				<?php
				$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
				$posts_per_page = 4;

				$args = array(
				    'posts_per_page' => $posts_per_page,
				    'cat' => 51,
				    'paged' => $paged,
				);
				$query = new WP_Query( $args );

				$last_page = $query->max_num_pages;
				$post_count = $query->found_posts;

				while ($post_count >= $posts_per_page) {
					$post_count = $post_count - $posts_per_page;
				}

				if($paged != $last_page){
					//Svi page-evi osim zadnjeg
					for($j = 0; $j <2 ;$j++){
						if ( $query->have_posts() ) { ?>
							<div class="row justify-content-center">
								<ul class="novostiPostUL novostiPageList">
								<?php
									for($i=0; $i<2; $i++){
										if ( $query->have_posts() ) {
											$query->the_post(); ?>
											<li class="novostiPostLI">
												<div class="post">
													<div class="entry">
														<div class="novostiImage">
															<?php if(get_field('istaknuta_slika')){ ?>
																<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><img src="<?php the_field('istaknuta_slika'); ?>"></a>
															<?php }else{ ?>
																<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><img src="../wp-content/themes/wp-bootstrap-starter/images/abeceda-default-slika.jpg"></a>
															<?php } ?>
														</div>

														<div class="novost-content novostiContent">
															<div class="content-height">
																<h4><a class="linkTitle" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
																<small class="objavljenoPost">Objavljeno:<b> <?php the_time( 'j. F Y' ); ?></b> </small><br><br>
																<?php
																	the_field("kratki_opis");
																?>
																<br>
																<a href="<?php the_permalink() ?>"><img class="novosti-vise" src="<?php the_field('glifikon_2', 15600); ?>"></a>
															</div>
														</div>
													</div>
												</div> <!-- closes the first div box -->
											</li>
									<?php }
									} ?>
								</ul>
							</div>
							<?php
							wp_reset_postdata();
						}
						 else {
						 ?>
						 <p><?php //esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
					 <?php } ?>

		<?php }
				}else{
					//zadnji page
					$posts_per_page = $post_count;
					//ako je na zadnjem page-u i ima 3 posta
					if($posts_per_page == 3){
						for($j = 0; $j <2 ;$j++){
							if ( $query->have_posts() ) { ?>
								<div class="row justify-content-center">
									<ul class="novostiPostUL novostiPageList">
									<?php
										for($i=0; $i<2; $i++){
											if ( $query->have_posts() ) {
												$query->the_post(); ?>
												<li class="novostiPostLI">
													<div class="post">
														<div class="entry">
															<div class="novostiImage">
																<?php if(get_field('istaknuta_slika')){ ?>
																	<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><img src="<?php the_field('istaknuta_slika'); ?>"></a>
																<?php }else{ ?>
																	<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><img src="../wp-content/themes/wp-bootstrap-starter/images/abeceda-default-slika.jpg"></a>
																<?php } ?>
															</div>

															<div class="novost-content novostiContent">
																<div class="content-height">
																	<h4><a class="linkTitle" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
																	<small class="objavljenoPost">Objavljeno:<b> <?php the_time( 'j. F Y' ); ?></b> </small><br><br>
																	<?php
																		the_field("kratki_opis");
																	?>
																	<br>
																	<a href="<?php the_permalink() ?>"><img class="novosti-vise" src="<?php the_field('glifikon_2', 15600); ?>"></a>
																</div>
															</div>
														</div>
													</div> <!-- closes the first div box -->
												</li>
										<?php }
										} ?>
									</ul>
								</div>
								<?php
								wp_reset_postdata();
							}
							 else {
							 ?>
							 <p><?php //esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
						 <?php } ?>

					<?php }
					//ako je na zadnjem postu i ima 2 posta
					}elseif ($posts_per_page == 2) {
						if ( $query->have_posts() ) { ?>
							<div class="row justify-content-center">
								<ul class="novostiPostUL novostiPageList">
								<?php
									for($i=0; $i<2; $i++){
										if ( $query->have_posts() ) {
											$query->the_post(); ?>
											<li class="novostiPostLI">
												<div class="post">
													<div class="entry">
														<div class="novostiImage">
															<?php if(get_field('istaknuta_slika')){ ?>
																<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><img src="<?php the_field('istaknuta_slika'); ?>"></a>
															<?php }else{ ?>
																<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><img src="../wp-content/themes/wp-bootstrap-starter/images/abeceda-default-slika.jpg"></a>
															<?php } ?>
														</div>

														<div class="novost-content novostiContent">
															<div class="content-height">
																<h4><a class="linkTitle" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
																<small class="objavljenoPost">Objavljeno:<b> <?php the_time( 'j. F Y' ); ?></b> </small><br><br>
																<?php
																	the_field("kratki_opis");
																?>
																<br>
																<a href="<?php the_permalink() ?>"><img class="novosti-vise" src="<?php the_field('glifikon_2', 15600); ?>"></a>
															</div>
														</div>
													</div>
												</div> <!-- closes the first div box -->
											</li>
									<?php }
									} ?>
								</ul>
							</div>
							<?php
							wp_reset_postdata();
						}
						 else {
						 ?>
						 <p><?php //esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
					 <?php }
					}elseif ($posts_per_page == 1) {
						if ( $query->have_posts() ) { ?>
							<div class="row justify-content-center">
								<ul class="novostiPostUL novostiPageList">
								<?php
										if ( $query->have_posts() ) {
											$query->the_post(); ?>
											<li class="novostiPostLI">
												<div class="post">
													<div class="entry">
														<div class="novostiImage">
															<?php if(get_field('istaknuta_slika')){ ?>
																<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><img src="<?php the_field('istaknuta_slika'); ?>"></a>
															<?php }else{ ?>
																<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><img src="../wp-content/themes/wp-bootstrap-starter/images/abeceda-default-slika.jpg"></a>
															<?php } ?>
														</div>

														<div class="novost-content novostiContent">
															<div class="content-height">
																<h4><a class="linkTitle" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
																<small class="objavljenoPost">Objavljeno:<b> <?php the_time( 'j. F Y' ); ?></b> </small><br><br>
																<?php
																	the_field("kratki_opis");
																?>
																<br>
																<a href="<?php the_permalink() ?>"><img class="novosti-vise" src="<?php the_field('glifikon_2', 15600); ?>"></a>
															</div>
														</div>
													</div>
												</div> <!-- closes the first div box -->
											</li>
									<?php } ?>
								</ul>
							</div>
							<?php
							wp_reset_postdata();
						}
						 else {
						 ?>
						 <p><?php // esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
					 <?php }
					}
				} ?>

			</div>
			<!--END OF NOVOSTI-->

			 <!-- PRESS -->
			<!--<div class="col-4">-->
			<div class="col-12 col-xl-4 press-stupac">
				<?php
					$paged2 = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
					$args2 = array(
						'posts_per_page' => 8,
						'cat' => 52,
						'paged' => $paged2,
					);
					$query2 = new WP_Query( $args2 );
					if ( $query2->have_posts() ) {
						?>
						<div class="contentShadow pressMargin">
						<span id="pressNaslov">Press</span>

							<hr class="borderPress">
							<?php

							if ( $query2->have_posts() ) {
								while ( $query2->have_posts() ){
									$query2->the_post(); ?>

									<div class="postPress">
										<div class="pressPadding">
											<!-- Display the date (November 16th, 2009 format) and a link to other posts by this posts author. -->
											<small class="pressDate"><?php the_time( 'j. F Y' ); ?> </small>

											<?php if(get_field('video_youtube') || get_field('video_upload')){ ?>
													<span class="play-btn">
														<img src="<?php bloginfo('url') ?>/wp-content/themes/wp-bootstrap-starter/images/video-play1.jpg" >
													</span>
											<?php } ?>

											<!-- Display the Title as a link to the Post's permalink. -->
											<h4 class="h4-inline"><a class="linkTitle" href="<?php the_permalink(); ?>" rel="bookmark" title="Poveznica na <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
										</div>
										<hr>
									</div> <!-- closes the first div box -->

						  <?php }
							} ?>
						</div>
			  <?php //wp_reset_postdata();
					}
					 else {
					 ?>
					 <p><?php //esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php } ?>
			</div>
			<!--END OF PRESS-->

		</div>	<!--END row -->
		<hr class="borderPress1">



<!--PAGINATOR-->

<div class="pagination-row">
	<div class="pagination">
	    <?php
	        echo paginate_links( array(
	            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
	            'total'        => $query->max_num_pages,
	            'current'      => max( 1, get_query_var( 'paged' ) ),
	            'format'       => '?paged=%#%',
	            'show_all'     => false,
	            'type'         => 'plain',
	            'end_size'     => 2,
	            'mid_size'     => 1,
	            'prev_next'    => true,
	            'prev_text'    => sprintf( '<i></i> %1$s', __( '<', 'text-domain' ) ),
	            'next_text'    => sprintf( '%1$s <i></i>', __( '>', 'text-domain' ) ),
	            'add_args'     => false,
	            'add_fragment' => '',
	        ) );
	    ?>
	</div>
</div>



<?php
get_footer();
?>
