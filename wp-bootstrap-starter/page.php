<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

<?php if( have_rows('slider', 15600) ){ ?>
		<div class="your-class">
			<?php while( have_rows('slider', 15600) ){
					the_row(); 	?>
					<div>
						<?php if(get_sub_field('slider_slika')){ ?>
										<img src="<?php the_sub_field('slider_slika'); ?>" width="100%" height="auto" />
						<?php } ?>
						<?php if(get_sub_field('tekst_na_slideru')){ ?>

							<div class="slider-tekst">
								<?php the_sub_field('tekst_na_slideru'); ?>
								<?php if(get_sub_field('dugme_url') && get_sub_field('dugme_tekst')){ ?>
								<a class="slider-button" href="<?php the_sub_field('dugme_lokacija'); ?>"><?php the_sub_field('dugme_tekst'); ?></a>
								<?php } ?>
							</div>
				<?php } ?>
				 </div>

			<?php } ?>
		</div>
<?php } ?>

<div id="content" class="site-content">
	<div class="container" id="contentID">
		<div class="row novostiPost">
			<h1 class="naslov-h1"><?php the_title(); ?></h1>
			<div class="container projekt-container defaultni">
				<?php the_field('sadrzaj'); ?>
				<hr class="borderPress1 projektHRpadding">
			</div>







	<?php /*<section id="primary" class="content-area col-sm-12 col-lg-8">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</section><!-- #primary --> */?>

<?php
//get_sidebar();
get_footer();
